let elem = document.createElement("p");
let textElem = document.createTextNode("我是一个新的文本");
elem.append(textElem);
// "<p>我是一个新的文本</p>"
document.getElementById("div1").appendChild(elem);
let childNode = document.getElementById("p1");
document.getElementById("div1").insertBefore(elem, childNode);
// <div id="div1">
//     <p id="p1">这是一个段落</p>
//     <p id="p2">这是另一个段落</p>
//     <p>我是一个新的文本</p>
// </div>
let child = document.getElementById("p1")
document.getElementById("div1").removeChild(child);

let newChild = document.createElement("p");
let textChild = document.createTextNode("我是更改后的元素");
newChild.append(textChild);
document.getElementById("div1").replaceChild(newChild, document.getElementById("p2"));

function gotoBaidu(){
    window.open("https://baidu.com");
}

function closeThis() {
    window.close();
}

function sayHello() {
    alert("hello")
}

function getTime(){
    d = new Date();
    document.getElementById("time").innerText = d.toLocaleTimeString();
}

let times = setInterval(getTime, 1000);
function stopTime(){
    clearInterval(times);
}

console.log(document.cookie)

// document.cookie = "username=Steve Jobs; expires=Mon, 12 Apr 2021 12:00:00 UTC"

document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/htmlstudy/4_11;";

function ajaxTest(){
    let a = new XMLHttpRequest()
    a.onreadystatechange = function (){
        if (a.status===200 && a.readyState === 4){
            console.log(a.responseText);
        }
    }
    a.open("get", "http://34.239.255.132:8099/comments",true);
    a.send();
}


$("#onAjax").click(function (){
    $.ajax({
        type:"post",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify({'name': 'zyf'}),
        url: "http://34.239.255.132:8099/comments",
        success: function (response){
            console.log(response);
        },
        error: function (e){
            console.log(e.status);
        }
    })
})


