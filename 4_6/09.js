var fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.sort());
console.log(fruits.reverse());

var points = [40, 100, 1, 4, 5, 8, 12];
// 匿名函数 正排前减后 倒排后减前
console.log(points.sort(function (a, b){return a - b}));
console.log(points.sort(function (a, b){return b - a}));

// 递归算法 前后比大小 后面的继续跟再后面的比大小
// 注意，当比到负值的时候，本轮排序立即结束
// 第一轮排序：40 100 -60 => 40 100
// 第二轮排序：100 1 99 => 1 40 100
// 第三轮排序：1 4 -3 => 1 4 40 100
// 第四轮排序：4 5 -1 => 1 4 5 40 100
// 第五轮比较：5 8 -3 => 1 4 5 8 40 100
// 第六轮比较：8 12 -4 => 1 4 5 8 12 40 100

// ... 可变长参数
console.log(Math.max(...points));
console.log(Math.min(...points));

times = new Date().getHours()
if (times <= 10){
    console.log("早上好");
} else if (times > 10 && times <= 14){
    console.log("中午好");
}  else {
    console.log("下午好");
}

// 1、case必须加括号
// 2、要加default 强制要求
switch (times){
    case 10 :{
        console.log("早上好");
    }break;
    case 22 :{
        console.log("晚上好");
    }break;
    default:{
        console.log("你好");
    }
}

console.log(typeof times);
console.log(typeof fruits);
console.log(typeof "zyf");

console.log(new Date());

console.log(new Date(2021, 4, 6, 12, 12, 0, 100));

