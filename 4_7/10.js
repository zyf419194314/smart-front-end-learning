var num = 1

// RangeError
// num.toPrecision(500);
try {
    // 保留有效数字
    num.toPrecision(500);
} catch (e) {
    console.log(e.name);
    console.log(e.message);
}

var x;
try {
    x = y + 1;
} catch (e) {
    console.log(e.name);
}

// 语法错误需要合理构造，否则会被ide或者浏览器解析引擎直接发现，同理，语法错误是最有可能被直接发现并避免的错误
// try {
//     a = "123123123123
// } catch (e){
//     console.log(e.name);
// }

try {
    num.toUpperCase();
} catch (e) {
    console.log(e.name);
}

try {
    decodeURI("%%%");
} catch (e){
    console.log(e.name);
}

// var x = this
// console.log(x)

const PI = 3.141592654
PI = 3.14
PI = 3.1415
