var str1 = "The full name of China is the People's Republic of China";
console.log('indexOf方法:' + str1.indexOf('China'));
console.log('lastIndexOf方法:' + str1.lastIndexOf('China'));
console.log('indexOf方法(带起始位置):' + str1.indexOf('China', 19));
console.log('search方法:' + str1.search('China'));

var str2 = "Apple,Banana,Mango";
console.log('slice方法:' + str2.slice(6, 12));
console.log('substr方法:' + str2.substr(6, 6));
console.log('replace方法:' + str2.replace('Banana', 'zyf'));
console.log('toUpperCase方法:' + str2.toUpperCase());
console.log('toLowerCase方法:' + str2.toLowerCase());
console.log('charAt方法:' + str2.charAt(6));
console.log('charCodeAt方法:' + str2.charCodeAt(6));

var str3 = "Apple Banana Mango";
console.log('split方法:' + str3.split(" "));

var num1 = 9.5655
console.log('toExponential方法', num1.toExponential(2));
console.log('toExponential方法(保留9位)', num1.toExponential(9));
console.log('toFixed方法:', num1.toFixed(2));
console.log('toPrecision方法:', num1.toPrecision(3));

var bool1 = true;
console.log('Number方法:', Number(bool1));

var fruits = ['Apple', 'Banana', 'Orange', 'Mango'];
for (var i = 0; i < fruits.length; i++){
    console.log(fruits[i])
}
// 迭代器
fruits.forEach(eachTest);
function eachTest(value){
    console.log(value);
}

fruits.push('Lemon');
fruits.forEach(eachTest);
console.log(fruits.toString());
console.log(fruits.pop());
console.log(fruits.toString());
console.log(fruits.shift());
console.log(fruits.toString());
console.log(fruits.unshift('Apple'));
console.log(fruits.toString());

var arr = []
arr.push(1);
arr.push(2);
arr.push(3);
console.log(arr.toString());
console.log(arr.pop());
console.log(arr.pop());
console.log(arr.pop());

var arr1 = ['zyf1', 'zyf2'];
var var1 = fruits.concat(arr1);
console.log(var1);
// 指定起始位置
console.log(var1.slice(1));
// 指定结束位置
console.log(var1.slice(1, 4));




