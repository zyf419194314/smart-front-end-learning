function gotoAjax(){
    let ajax = new XMLHttpRequest();
    ajax.open(
        "get",
        "http://34.239.255.132:8099/posts")
    ajax.onreadystatechange = function (){
        // ajax.status 状态码
        // 200 请求正常
        // 301 请求重定向 临时重定向
        // 302 永久重定向
        // 403 服务器拒绝响应 鉴权失败
        // 404 请求资源不存在
        // 500 服务器异常
        // 500.1 500.2
        // TCP/IP 协议的三次握手
        // 0-4
        // TCP协议第一次握手1 客户端发起 -> 服务器 0 我要发起一个连接了
        // TCP协议第一次握手2 服务端发起 -> 客户端 1 我正常，我准备好了
        // TCP协议的第二次握手1 客户端发起 -> 服务端 2 我准备传多少字节的数据
        // TCP协议的第二次握手2 服务端发起 -> 客户端 3 我准备好了，我也有这么多空间，我能行，发吧
        // TCP协议的第三次握手1 客户端发起 -> 服务端 4 我发数据了了，请接收
        if (ajax.status === 200 && ajax.readyState === 4){
            console.log(ajax.responseText)
        }
    }
    ajax.send()
}

$("#jqajax").click(function (){
    $.ajax({
        type: "post",
        contentType: "application/json;charset=UTF-8",
        url: "http://34.239.255.132:8099/posts",
        data: JSON.stringify({"name": "zyf"}),
        success:function (response){
            console.log(response)
        }
    })
})
