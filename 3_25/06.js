// alert("第一个script");
// alert(5 + 6);

// js标准要求必须加封号，否则无法使用
// ES标注 ES5 ES6 明确尽量不要加封号，也即不推荐使用封号作为一行程序的结尾
// 现代浏览器 2016年之后的浏览器都支持换行作为一行代码的结束
document.getElementById("demo").innerHTML = "我已经被修改了";
document.write(Date());

a = 5;
b = 6;
console.log(5 + 6);

console.log(3e5);

// console.log("zyf")

var name = 'zyf';
console.log(name);

function logNum(){
    console.log(a + b);
}

logNum();
