
solution1([1,2,3,4,5,5])


function solution1(nums){
    var list1 = [], list2 = [];
    var count = 0;
    for (var i = 0 ; i < nums.length; i++){
        // 查找元素是否在list1里
        inArray = list1.indexOf(nums[i]);
        // 如果不在则添加一个索引
        // 如果在，对应的索引自增
        if (inArray !== -1){
            list2[inArray] ++;
        } else {
            list1.push(nums[i]);
            list2.push(1);
        }
    }

    for (var i = 0 ; i < list2.length; i++){
        if (list2[i] === 1){
            count += list1[i];
        }
    }

    console.log(count);

}
