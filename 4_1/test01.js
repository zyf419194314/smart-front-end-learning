// 给你一个偶数长度的字符串 s 。将其拆分成长度相同的两半，前一半为 a ，后一半为 b 。
// 两个字符串 相似 的前提是它们都含有相同数目的元音（'a'，'e'，'i'，'o'，
// 'u'，'A'，'E'，'I'，'O'，'U'）。注意，s 可能同时含有大写和小写字母。
// 如果 a 和 b 相似，返回 true ；否则，返回 false 。

solution2("aaaiiiip");

// 以空间换时间
// O(n)时间复杂度
function solution1(s){
    // 字符串切片，分为两个字符串
    var half = s.length/2;
    var s1 = s.slice(0, half);
    var s2 = s.slice(half);
    // 定义一个计数器
    var count1 = 0, count2 = 0;

    // for循环，通过指定条件进行计数器的累加
    for (var i = 0 ; i < half; i++){
        if (s1[i] === 'a' || s1[i] === 'e' || s1[i] === 'i' || s1[i] === 'o' || s1[i] === 'u' || s1[i] === 'A' || s1[i] === 'E' || s1[i] === 'I' || s1[i] === 'O' || s1[i] === 'U'){
            count1++;
        }
        if (s2[i] === 'a' || s2[i] === 'e' || s2[i] === 'i' || s2[i] === 'o' || s2[i] === 'u' || s2[i] === 'A' || s2[i] === 'E' || s2[i] === 'I' || s2[i] === 'O' || s2[i] === 'U'){
            count2++;
        }
    }

    // 展示结果
    if (count1 === count2){
        console.log("true");
    } else {
        console.log("false");
    }
}

// 以时间换空间
function solution2(s){
    // 字符串切片，分为两个字符串
    var half = s.length/2;
    var s1 = s.slice(0, half);
    var s2 = s.slice(half);
    // 定义一个目标数组
    var target = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
    // 加强for循环
    for (var i in target){
        s1 = s1.replaceAll(target[i], '');
        s2 = s2.replaceAll(target[i], '');
    }

    if (s1.length === s2.length){
        console.log('true');
    } else {
        console.log('false');
    }
}


